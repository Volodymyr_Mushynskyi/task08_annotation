package com.mushynskyi.entity;

import com.mushynskyi.annotations.MyAnnotation;
import com.mushynskyi.annotations.NameFieldAnnotation;
import com.mushynskyi.annotations.VolumeFieldAnnotation;

@MyAnnotation
public class Box {

  @NameFieldAnnotation(name = "A")
  public String name;

  @VolumeFieldAnnotation(value = 25)
  public Integer volume;

  public Box() {

  }

  public Box(String name, Integer volume) {
    this.name = name;
    this.volume = volume;
  }

  public void setVolume(Integer volume) {
    this.volume = volume;
  }

  public String getName() {
    return name;
  }

  public Integer getVolume() {
    return this.volume;
  }

  public String myMethods(String string, int... args) {
    return "myMethods(String string, int...args)";
  }

  public String myMethods(String... string) {
    return "myMethods(String...string)";
  }
}
