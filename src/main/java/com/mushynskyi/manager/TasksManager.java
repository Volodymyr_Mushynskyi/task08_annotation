package com.mushynskyi.manager;

import com.mushynskyi.annotations.NameFieldAnnotation;
import com.mushynskyi.annotations.VolumeFieldAnnotation;
import com.mushynskyi.entity.Box;
import com.mushynskyi.entity.GenericClass;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TasksManager {
  private static Logger logger = LogManager.getLogger(TasksManager.class);

  public void printFields() {
    try {
      new TasksManager().printAnnotation();
      logger.info("Default value");
      new TasksManager().printNameAnnotationValue();
      new TasksManager().printVolumeAnnotationValue();
      new TasksManager().invokeMethod();
      new TasksManager().setUnknownField();
      new TasksManager().invokeTwoMethods();
    } catch (NoSuchFieldException | NoSuchMethodException | InvocationTargetException | IllegalAccessException | InstantiationException e) {
      logger.fatal("You catch exception");
    }
    new TasksManager().printGenericTask();
  }

  private void printAnnotation() throws NoSuchFieldException {
    logger.info("Print Annotation");
    Class aClass = Box.class;
    Field firstField = aClass.getField("name");
    Annotation firstAnnotation = firstField.getAnnotation(NameFieldAnnotation.class);
    Field secondField = aClass.getField("volume");
    Annotation secondAnnotation = secondField.getAnnotation(VolumeFieldAnnotation.class);
    NameFieldAnnotation myFieldAnnotation = (NameFieldAnnotation) firstAnnotation;
    logger.trace("name: " + myFieldAnnotation.name());
    VolumeFieldAnnotation myVolumAnnotation = (VolumeFieldAnnotation) secondAnnotation;
    logger.trace("value: " + myVolumAnnotation.value());
  }

  private void printNameAnnotationValue() throws NoSuchMethodException {
    Class<?> clazz = NameFieldAnnotation.class;
    Method method = clazz.getDeclaredMethod("name");
    String value = (String) method.getDefaultValue();
    logger.trace("default :" + value);
  }

  private void printVolumeAnnotationValue() throws NoSuchMethodException {
    Class<?> clazz = VolumeFieldAnnotation.class;
    Method method = clazz.getDeclaredMethod("value");
    Integer value = (Integer) method.getDefaultValue();
    logger.trace("default :" + value);
  }

  private void invokeMethod() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    logger.info("Invoke three methods");
    Method setMethod = Box.class.getMethod("setVolume", Integer.class);
    logger.trace(setMethod);
    Box operationsInstance = new Box("B", 11);
    setMethod.invoke(operationsInstance, 3);
    Method getNameMethod = Box.class.getMethod("getName");
    logger.trace(getNameMethod);
    String stringResult = (String) getNameMethod.invoke(operationsInstance);
    logger.trace(stringResult);
    Method getVolumeMethod = Box.class.getMethod("getVolume");
    logger.trace(getVolumeMethod);
    Integer result = (Integer) getVolumeMethod.invoke(operationsInstance);
    logger.trace(result);
  }

  private void setUnknownField() throws NoSuchFieldException, IllegalAccessException, InstantiationException {
    logger.info("Set unknown field");
    Class<?> clazz = Box.class;
    Object type = clazz.newInstance();
    Field field = clazz.getDeclaredField("name");
    field.setAccessible(true);
    field.set(type, "Unknown");
    String str1 = (String) field.get(type);
    logger.trace("field: " + str1);
  }

  private void invokeTwoMethods() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    logger.info("Invoke two methods");
    Method setMethod = Box.class.getMethod("myMethods", new Class[]{String[].class});
    logger.trace(setMethod);
    String[] strings = new String[]{"c", "f", "k"};
    String string = (String) setMethod.invoke(new Box(), new Object[]{strings});
    logger.trace(string);
    Method setMethod2 = Box.class.getMethod("myMethods", String.class, int[].class);
    logger.trace(setMethod2);
    int[] integers = new int[]{1, 2, 3, 4, 5, 6};
    string = (String) setMethod2.invoke(new Box(), "hello", integers);
    logger.trace(string);
  }

  private void printGenericTask() {
    logger.info("Information about generic class");
    new GenericClass<>("G");
    Class<?> clazz = GenericClass.class;
    String string = clazz.getName();
    logger.trace(string);
    Field[] fields = clazz.getFields();
    for (Field string1 : fields) {
      logger.trace(string1);
    }
    Method[] methods = clazz.getMethods();
    for (Method string1 : methods) {
      logger.trace(string1);
    }
    Constructor[] constructors = clazz.getConstructors();
    for (Constructor string1 : constructors) {
      logger.trace(string1);
    }
  }
}
